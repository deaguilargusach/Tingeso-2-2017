app.controller('MainController', ['$scope','$location', function($scope,$location) {

	$scope.title = "Sistema de Programación Python";

  
    $scope.studentPage = $location.path() === '/student';
 

	$scope.navigation = [
	
	{"text":"Ver Estudiantes",   "link":"#!/student", "condition":$scope.studentPage},
	
	];

}]);